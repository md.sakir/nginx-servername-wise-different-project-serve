# Servername wise different project serve

### For example example1.com will serve project1 & example2.com will serve project2

### Make sure you have docker, docker-compose installed & host 80 port is available

## Configure for local environment

```
sudo nano /etc/hosts
127.0.0.1   project1.com
127.0.0.1   project2.com
ctrl + s
ctrl + x
```

### Run bellow command in the root of this project

```
docker-compose up -d
```

### Let's check output in browser

```
http://project1.com
http://project2.com
```

## Configure for live environment (You must have a dedicated public ip addess in your host machine)

- Go to your domain control panel (Example namecheap, godady, etc)
- Go to DNS management area
- Add 'A' 2 records for each domain
- Use @ ---- (your ip) & wwww ----- (your ip)

### You need to check your hostmachine's firewall. Disable firewall or allow ports for extranal access

#### For ubuntu os

- 'ufw disable' or 'ufw allow 80'

## If you under a wifi network you have to check wifi router's port configuration

- Login your router control panel
- Find port DHCP Server/Port forward, use port 80 for internal and extranal and set your host machine's ip

### For SSL/HTTPS use port 443

## For more support contact me

- engr.sakir@gmail.com
- 8801304734623
